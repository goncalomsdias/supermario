################
### Provider ###
################

# Provider Region
variable "provider_profile" {
  default = "default"
}

variable "provider_region" {
  default = "eu-west-1"
}

#################
### Terraform ###
#################

# AWS SSH Key - (Set AWS instance Key Name, AWS Public Key File)
variable "key_pair_name" {
  default = "Supermario"
}

variable "ssh_key_private" {
  default = "git/supermario/ansible/key/supermario.pem"
}

# Instance name prefix
variable "instance_name" {
  default = "SuperMario"
}

# AWS AMI (Define the OS to deploy)
variable "instance_ami" {
  default = "ami-035966e8adab4aaad"
}

# AWS instance type (Define HW to use)
variable "instance_type" {
  default = "t2.micro"
}

# AWS instance user used in SSH connection
variable "connection_user" {
  default = "ubuntu"
}

# Number of instance to deploy
variable "instance_count" {
  default = 2
}

################
### Ansible ###
################

# Ansible Playbook Path
variable "playbook_path" {
  default = "git/supermario/ansible/deploy.yml"
}

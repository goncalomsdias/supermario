# Create a VPC to launch our instances into
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"

  tags = {
      Name = "SuperMario"
  }
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

# Create a subnet to launch our instances into
resource "aws_subnet" "default" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true

  tags = {
      Name = "SuperMario"
  }
}

# Security group to allow Internet access and internal HTTP traffic through the VPC
resource "aws_security_group" "supermariosg" {
    name        = "supermariosg"
    description = "Allow Internal HTTP and external Internet access"
    vpc_id = "${aws_vpc.default.id}"

    # Allow HTTP port 80 from the VPC
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
    }

    #Temporary SSH access for test purposes
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    # Outbound Rule
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "supermariosg"
    }
}

resource "aws_key_pair" "Supermario" {
  key_name   = "${var.key_pair_name}"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD4OpZozxmScH5UmDdbYZBf5Ye+o3xzbrq3IUq6zlDqSpDaZ1cYf+6cgqGSrXluX5chtbBVqDd6r6g8APEhhXq4n130dmvE9JWvqpWltjyTfy8KP1F++QvnL6RTdm9xXUBCLf6B6GE+ZXgkVqbK3JmFFRYnfjFbvyREYGZsRKMMudpTTmTmni/7UHNiZjhFOZoB5B6ULWSOftWmmcQJbiC2d79Czwa/3AUdg9LlwN9N3MQiTTQu1cQOD9DbPj3ugtH+QAZOz/lOsOHir2/da0btt1G3ZmVL69vdrMTqkbNMaujUQgXjXaFAiXdYfKdYwn3y83PaY5+dZEQfyRi1MCO9 goncalo@euromedia04"
}

#AWS EC2 instances
resource "aws_instance" "supermario-web" {
    ami           = "${var.instance_ami}"
    instance_type = "${var.instance_type}"
    key_name = "${var.key_pair_name}"
    security_groups = ["${aws_security_group.supermariosg.id}"]
    subnet_id = "${aws_subnet.default.id}"

    count = "${var.instance_count}"
    tags          = {
        Name        = "${var.instance_name}${count.index+1}"
    }

    #Launch remote-exec to make sure instance is up before running ansible playbook
    provisioner "remote-exec" {

	# Terraform initial instance command
        inline = [
            "sudo apt-get update -y"
        ]

	# Terraform instance conection options
        connection {
            host        = "${self.public_ip}"
            type        = "ssh"
            user        = "${var.connection_user}"
            private_key = "${file("${pathexpand("~/${var.ssh_key_private}")}")}" # pathexpand replace ~ segment with the current user home directory path
        }
    }

    # Add instance key to this hosts, execute ansible playbook using keys and write host data to file
    provisioner "local-exec" {
        command = "eval `ssh-agent -s` && ssh-add '${pathexpand("~/${var.ssh_key_private}")}' && export ANSIBLE_HOST_KEY_CHECKING=False && ansible-playbook -vv -u ubuntu -i '${self.public_ip},' --key-file=${var.ssh_key_private} '${pathexpand("~/${var.playbook_path}")}'"
    }
}

# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "elb" {
  name        = "supermario_elb"
  description = "ELB for SuperMario web instances"
  vpc_id      = "${aws_vpc.default.id}"

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "web" {
  name = "supermario-elb"

  subnets         = ["${aws_subnet.default.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = "${aws_instance.supermario-web.*.id}"

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
}

output "elb_name"{
  value = "${aws_elb.web.dns_name}"
  description = "ELB DNS Name"
}
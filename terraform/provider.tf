provider "aws" {
  profile    = var.provider_profile
  version    = "~> 2.23"
  region     = var.provider_region
}

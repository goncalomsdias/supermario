# supermario

Supermario deploy with terraform and ansible

**Setup instructions:**  
cd ~  
mkdir git  
cd git  
git clone https://gitlab.com/goncalomsdias/supermario.git  
export AWS_ACCESS_KEY_ID= [YourAccessKey]  
export AWS_SECRET_ACCESS_KEY= [YourSecretAccessKey]  
cd git/supermario/terraform  
terraform init  
terraform plan  
terraform apply  

**Custom Variables:**  
provider_region - *default = eu-west-1*  
key_pair_name - *default=Supermario*  
instance_name - *default = SuperMario*  
instance_ami - *default = ami-035966e8adab4aaad (Ubuntu 18.04)*  
instance_type - *default = t2.micro*  
connection_user - *default = ubuntu*  
instance_count - *default = 2*  
playbook_path - *default = git/supermario/ansible/deploy.yml*  

The custom variables can be changed in terraform/variables.tf


**After running the terraform apply command, the Output will contain elb_name, which will be the URL to access the service.**
**The URL can take one or two minutes to be up and responding to requests.**
